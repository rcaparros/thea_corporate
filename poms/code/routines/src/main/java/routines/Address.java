package routines;

public class Address {

	private String externalID;
	private String country;
	private String name;
	private String zip;
	private String city;
	private String county;
	private String brick;
    
	public Address(String externalID, String country, String name, String zip, String city, String county, String brick){
    	this.externalID = externalID;
    	this.country = country;
    	this.name = name;
    	this.zip = zip;
    	this.city = city;
    	this.county = county;
    	this.brick = brick;
    }
	
	public Address(){
		this("", "", "", "", "", "", "");
	}

	public String getExternalID() {
		if(externalID!=null)
			return externalID;
		else
			return "";
	}

	public void setExternalID(String externalID) {
		this.externalID = externalID;
	}

	public String getCountry() {
		if(country!=null)
			return country;
		else
			return "";
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getName() {
		if(name!=null)
			return name;
		else
			return "";
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getZip() {
		if(zip!=null)
			return zip;
		else
			return "";
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getCity() {
		if(city!=null)
			return city;
		else
			return "";
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCounty() {
		if(county!=null)
			return county;
		else
			return "";
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getBrick() {
		if(brick!=null)
			return brick;
		else
			return "";
	}

	public void setBrick(String brick) {
		this.brick = brick;
	}

	
	
}
